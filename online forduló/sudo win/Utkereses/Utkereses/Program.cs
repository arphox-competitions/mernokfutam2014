﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Utkereses
{
    class Program
    {

        
        static int[,] labirintus;
        static Queue<Pont> pontokSora;
        static Pont cel;
        static Pont start;

        static void Main(string[] args)
        {

            StreamReader sr = new StreamReader("labirintus.txt");
            labirintus = new int[SorokSzama(), OszlopokSzama()];

            pontokSora = new Queue<Pont>();

            string line = "";
            int i = -1;
            while (!sr.EndOfStream)
            {
                line = sr.ReadLine();
                i++;
                for (int j = 0; j < line.Length; j++)
                {
                    if (line[j] == '0')
                        labirintus[i, j] = -2;
                    else if (char.ToLower(line[j]) == 's')
                    {
                        labirintus[i, j] = 0;
                        pontokSora.Enqueue(new Pont(i, j));
                        start = new Pont(i, j);
                    }
                    else if (char.ToLower(line[j]) == 'c')
                    {
                        labirintus[i, j] = -4;
                        cel = new Pont(i, j, int.MaxValue);
                    }
                    else if (line[j] == ' ')
                        labirintus[i, j] = -1;
                }
            }
            sr.Close();

            Bejar(labirintus);
            

            UtakAdatai(labirintus);
            MasolUtak();


            for (i = 0; i < utak.Count; i++)
            {
                Animacio(utak.ElementAt(i), labirintus, i+1);
            }

            for (i = 0; i < utakMasolat.Count; i++)
            {
                Console.WriteLine("Az " + (i + 1) + ". legrövid ut adatai:");
                Console.WriteLine("Az út hossza: " + cel.SorSzam);
                Console.WriteLine("A fordulások száma: " + FordulasokSzama(utakMasolat.ElementAt(i)));
            }

            Console.ReadLine();
            
        }



        static void MasolUtak()
        {
            utakMasolat = new List<Stack<Pont>>();
            for (int i = 0; i < utak.Count; i++)
            {
                Stack<Pont> ut = new Stack<Pont>();
                for (int j = utak.ElementAt(i).Count - 1; j >= 0; j--)
                {
                    ut.Push(utak.ElementAt(i).ElementAt(j));
                    Console.WriteLine("asD");
                }
                utakMasolat.Add(ut);
            }
        }

        static List<Stack<Pont>> utakMasolat;
        static int FordulasokSzama(Stack<Pont> pontok)
        {
            int fordulasok = 0;
            if (pontok.Count > 1)
            {
                Pont aPont = pontok.Pop();
                Pont bPont = pontok.Pop();
                int x = aPont.X - bPont.X;
                int y = aPont.Y - bPont.Y;
                while (pontok.Count > 0)
                {
                    aPont = bPont;
                    bPont = pontok.Pop();
                    if (aPont.X - bPont.X != x || aPont.Y - bPont.Y != y)
                    {
                        fordulasok++;
                        x = aPont.X - bPont.X;
                        y = aPont.Y - bPont.Y;
                    }
                }
            }
            return fordulasok;
        }



        static void Animacio(Stack<Pont> a, int[,] b, int utSzama)
        {
            Console.Clear();
            Console.WriteLine("Legrövidebb út " + utSzama + "/" + utak.Count + ":");
            for (int i = 0; i < b.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    if (b[i, j] > 0 || b[i,j] == -3 || b[i,j] == -4)
                        b[i, j] = -1;
                }
            }
            b[start.X, start.Y] = -3;

            while (a.Count > 0)
            {
                Console.Clear();
                Console.WriteLine("Legrövidebb út " + utSzama + "/" + utak.Count + ":");
                Kiir2(b);
                Pont pont = a.Pop();
                if (pont.SorSzam == 0)
                    b[pont.X, pont.Y] = -3;
                else if (pont.SorSzam == cel.SorSzam)
                    b[pont.X, pont.Y] = -4;
                else
                    b[pont.X, pont.Y] = 1;
                Thread.Sleep(200);
            }
            Console.Clear();
            Console.WriteLine("Legrövidebb út " + utSzama + "/" + utak.Count + ":");
            Kiir2(b);
            if (utSzama < utak.Count) {
                Console.WriteLine("Enter a következő úthoz");
                Console.ReadLine();
            }
        }



        static void Kiir2(int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    if (a[i, j] == -1)
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write(' ');
                    }
                    else if (a[i, j] == -2)
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write('0');
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        if (a[i, j] == -3)
                            Console.Write('s');
                        else if (a[i, j] == -4)
                            Console.Write('c');
                        else
                            Console.Write(' ');
                    }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine();
            }
        }



        static List<Stack<Pont>> utak;
        static void UtakAdatai(int[,] a)
        {
            utak = new List<Stack<Pont>>();
            mostaniMelyseg = cel.SorSzam;
            BackTrackingesBejaras(cel, null);
        }


        static int mostaniMelyseg;
        static void BackTrackingesBejaras(Pont pont, Stack<Pont> ut)
        {

            if (pont.SorSzam >= mostaniMelyseg)
            {
                Stack<Pont> regiUt = ut;
                ut = new Stack<Pont>();
                utak.Add(ut);
                if (regiUt != null)
                    for (int i = regiUt.Count - 1; i >= 0; i--)
                        if (regiUt.ElementAt(i).SorSzam > pont.SorSzam)
                            ut.Push(regiUt.ElementAt(i));
                ut.Push(pont);
            }
            else
            {
                ut.Push(pont);
            }
            mostaniMelyseg = pont.SorSzam;

            if (BackTrackingnekMegfelel(new Pont(pont.X - 1, pont.Y),pont.SorSzam))
                BackTrackingesBejaras(new Pont(pont.X - 1, pont.Y, pont.SorSzam - 1),ut);
            if (BackTrackingnekMegfelel(new Pont(pont.X + 1, pont.Y), pont.SorSzam))
                BackTrackingesBejaras(new Pont(pont.X + 1, pont.Y, pont.SorSzam - 1),ut);
            if (BackTrackingnekMegfelel(new Pont(pont.X, pont.Y - 1), pont.SorSzam))
                BackTrackingesBejaras(new Pont(pont.X, pont.Y - 1, pont.SorSzam - 1),ut);
            if (BackTrackingnekMegfelel(new Pont(pont.X, pont.Y + 1), pont.SorSzam))
                BackTrackingesBejaras(new Pont(pont.X, pont.Y + 1, pont.SorSzam - 1),ut);
        }



        static bool BackTrackingnekMegfelel(Pont pont, int pontSorrendje)
        {
            if (pont.X < 0 || pont.Y < 0 || pont.X >= labirintus.GetLength(0) || pont.Y >= labirintus.GetLength(1))
                return false;
            if (labirintus[pont.X, pont.Y] >= 0 && labirintus[pont.X, pont.Y] == pontSorrendje - 1)
                return true;
            return false;
        }



        static void Kiir(int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    if (a[i, j] == -1)
                        Console.Write(' ');
                    else if (a[i, j] == -2)
                        Console.Write('0');
                    else if (a[i, j] == -3)
                        Console.Write('S');
                    else if (a[i, j] == -4)
                        Console.Write('C');
                    else
                        Console.Write(a[i, j]);
                Console.WriteLine();
            }
        }



        static void Bejar(int[,] a)
        {
            Pont pont = new Pont(0, 0);
            while (pontokSora.Count > 0)
            {
                pont = pontokSora.Dequeue();
                if (a[pont.X, pont.Y] != -3 && a[pont.X, pont.Y] != -4 && (a[pont.X, pont.Y] < 0 || a[pont.X, pont.Y] > pont.SorSzam))
                    a[pont.X, pont.Y] = pont.SorSzam;

                if (a[pont.X, pont.Y] != -4)
                {
                    if (Bejarhato(new Pont(pont.X - 1, pont.Y)))
                        pontokSora.Enqueue(new Pont(pont.X - 1, pont.Y, pont.SorSzam + 1));
                    if (Bejarhato(new Pont(pont.X + 1, pont.Y)))
                        pontokSora.Enqueue(new Pont(pont.X + 1, pont.Y, pont.SorSzam + 1));
                    if (Bejarhato(new Pont(pont.X, pont.Y - 1)))
                        pontokSora.Enqueue(new Pont(pont.X, pont.Y - 1, pont.SorSzam + 1));
                    if (Bejarhato(new Pont(pont.X, pont.Y + 1)))
                        pontokSora.Enqueue(new Pont(pont.X, pont.Y + 1, pont.SorSzam + 1));
                }
                else if (a[pont.X, pont.Y] == -4)
                {
                    if (pont.SorSzam < cel.SorSzam)
                        cel.SorSzam = pont.SorSzam;
                }
            }
        }



        static bool Bejarhato(Pont pont)
        {
            if (pont.X < 0 || pont.Y < 0 || pont.X >= labirintus.GetLength(0) || pont.Y >= labirintus.GetLength(1))
                return false;
            if ((labirintus[pont.X, pont.Y] == -1 || labirintus[pont.X, pont.Y] == -4))
                return true;
            return false;
        }



        static int SorokSzama()
        {
            StreamReader sr = new StreamReader("labirintus.txt");
            int db = 0;
            while (!sr.EndOfStream)
            {
                sr.ReadLine();
                db++;
            }
            sr.Close();
            return db;
        }



        static int OszlopokSzama()
        {
            StreamReader sr = new StreamReader("labirintus.txt");
            int max = 0;
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line.Length > max)
                    max = line.Length;
            }
            sr.Close();
            return max;
        }
    }
}
