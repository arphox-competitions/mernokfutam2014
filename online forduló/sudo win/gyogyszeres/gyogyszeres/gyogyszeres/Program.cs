﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace gyogyszeres
{
    class Program
    {


        static List<Gyogyszer> gyogyszerek = new List<Gyogyszer>();

        static void beolvas(string fajlnev)
        {
            StreamReader sr = new StreamReader(fajlnev);

            int i = 0;
            string sor;           
            int db=0;

            while (!sr.EndOfStream)
            {
                gyogyszerek.Add(new Gyogyszer());

                sor = (sr.ReadLine()).Replace(" ", string.Empty);
                
                if (sor.Contains('('))
                {
                    gyogyszerek[db].Nev = sor.Substring(0,sor.IndexOf('('));  ///-1
                    gyogyszerek[db].Minkor = int.Parse(sor.Substring(sor.IndexOf('(') + 1, ((sor.IndexOf(')') - sor.IndexOf('(')) - 1)));
                }
                else
                {
                    gyogyszerek[db].Nev = sor.Substring(0, sor.IndexOf(';'));
                    gyogyszerek[db].Minkor=0;
                }

                 sor=sor.Remove(0,sor.IndexOf(':')+1); // levágjuk hatas: <--ig marad laz,fejfajas...
                 
                 if (sor.Contains("mellekhatas"))
                 {

                     string[] subhatasok = (sor.Substring(0, sor.IndexOf(';'))).Split(',');
                     for (i = 0; i < subhatasok.Length; i++) // hatások hozzáadása
                     {
                         gyogyszerek[db].hatasHozzaad(subhatasok[i]);
                     }
                     sor = sor.Remove(0,sor.IndexOf(':')+1);
                     subhatasok = sor.Split(',');
                     for (i = 0; i < subhatasok.Length; i++) // mellékhatások hozzáadása
                     {
                         gyogyszerek[db].mellekhatasHozzaad(subhatasok[i]);
                     }
                     
                 }
                 else
                 {
                     string[] subhatasok = sor.Split(',');
                     for (i = 0; i < subhatasok.Length; i++)
                     {
                         gyogyszerek[db].hatasHozzaad(subhatasok[i]);
                     }
                 }

                db++;
            }
            sr.Close();
        }




        static int eletkor;
        static List<int> megoldas = new List<int>();
        static List<int> legjobbMegoldas = new List<int>();
        static List<List<string>> tunetek;
        static int legjobbMegoldasSzint;
        static void Main(string[] args)
        {
            string panaszok = "";
            string nev = "";
            eletkor = 0;
            beolvas("gyogyszerek.txt");

            Console.Write("Kérem írja be a nevét: ");
            nev = Console.ReadLine();
            Console.Write("Adja meg az életkorát: ");
            eletkor = int.Parse(Console.ReadLine());
            Console.Write("Kérem írja be a panaszait (vesszővel elválasztva, szóközök nélkül: ");
            panaszok = Console.ReadLine();


            tunetek = new List<List<string>>();
            string[] tunetekTomb = panaszok.Split(',');
            List<string> ujTunetek = new List<string>();
            for (int i = 0; i < tunetekTomb.Length; i++)
                ujTunetek.Add(tunetekTomb[i]);
            tunetek.Add(ujTunetek);

            bool van = false;
            megoldas = new List<int>();
            legjobbMegoldas = new List<int>();
            legjobbMegoldasSzint = int.MaxValue;

            BackTrack(0, ref van,megoldas, ref legjobbMegoldas);

            if (legjobbMegoldas.Count == 0)
            {
                Console.WriteLine("Gyógyíthatatlan");
            }
            else
            {
                Console.Write("A legjobb megoldás " + (legjobbMegoldasSzint + 1) + " darab gyógyszerrel érhető el: ");
                for (int i = 0; i < legjobbMegoldasSzint; i++)
                {
                    Console.Write(gyogyszerek.ElementAt(legjobbMegoldas.ElementAt(i)).Nev + ", ");
                }
                Console.Write(gyogyszerek.ElementAt(legjobbMegoldas.ElementAt(legjobbMegoldasSzint)).Nev);
            }
            Console.ReadLine();
            
        }




        static void BackTrack(int szint, ref bool van, List<int> megoldas, ref List<int> legjobbMegoldas)
        {
            int i = -1; //hany gyogyszert hasznalok mar most
            do
            {
                i++;
                if (ft(i, szint)) //ev szerint beveheti-e es egyaltalan hasznal-e
                {
                    int k = 0; 
                    while (k < szint && fk(szint, k, i)){
                        k++;
                    }
                    if (k == szint) 
                    {
                        if (megoldas.Count < szint + 1)
                            megoldas.Add(i); 
                        else
                            megoldas[szint] = i; 

                        if (Megoldase(szint))
                        {
                            if (!van || (szint < legjobbMegoldasSzint && legjobbMegoldasSzint != int.MaxValue))
                            {
                                legjobbMegoldas = new List<int>();
                                Atmasol2(legjobbMegoldas, megoldas);
                                legjobbMegoldasSzint = szint;
                            }
                            van = true;
                        }
                        else if (szint < legjobbMegoldasSzint - 1 && szint < gyogyszerek.Count - 1)
                            BackTrack(szint + 1, ref van, megoldas, ref legjobbMegoldas);
                    }
                }
            } while (i < gyogyszerek.Count - 1);
        }



        static bool Megoldase(int szint)
        {
            List<string> a = new List<string>();
            Atmasol(a, tunetek.ElementAt(0));
            
            for (int i = 0; i <= szint; i++)
            {
                Gyogyszer gyogyszer = gyogyszerek.ElementAt(megoldas[i]);
                for (int j = 0; j < gyogyszer.mellekhatasokMeret(); j++)
                {
                    string mellekhatas = gyogyszer.mellekhatasElem(j);
                    if (!a.Contains(mellekhatas))
                        a.Add(mellekhatas);
                }
            }
            
            for (int i = 0; i <= szint; i++)
            {
                Gyogyszer gyogyszer = gyogyszerek.ElementAt(megoldas[i]);
                for (int j = 0; j < gyogyszer.hatasokMeret(); j++)
                {
                    string hatas = gyogyszer.hatasElem(j);
                    if (a.Contains(hatas))
                    {
                        a.Remove(hatas);
                    }
                }
            }

            return (a.Count == 0);
        }



        static void Atmasol(List<string> a, List<string> b)
        {
            for (int i = 0; i < b.Count; i++)
                a.Add(b.ElementAt(i));
            
        }



        static void Atmasol2(List<int> a, List<int> b)
        {
            for (int i = 0; i < b.Count; i++)
                a.Add(b.ElementAt(i));
        }



        static bool fk(int szint, int k, int i)
        {
            return (megoldas.Count <= szint || megoldas.ElementAt(k) != i);
        }



        static bool ft(int i, int szint)
        {
            return (gyogyszerek.ElementAt(i).Minkor <= eletkor);
        }
    }


}
