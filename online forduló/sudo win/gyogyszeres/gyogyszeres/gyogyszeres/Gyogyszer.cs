﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gyogyszeres
{
    public class Gyogyszer
    {
        private string nev;
        public string Nev
        {
            get
            {
                return nev;
            }
            set
            {
                nev = value;
            }
        }
        private int minkor;
        public int Minkor
        {
            get
            {
                return minkor;
            }
            set
            {
                minkor = value;
            }
        }

        private List<string> hatasok = new List<string>();
        private List<string> mellekhatasok = new List<string>();

        public void hatasHozzaad(string hatas)
        {
            hatasok.Add(hatas);
        }

        public void mellekhatasHozzaad(string mellekhatas)
        {
            mellekhatasok.Add(mellekhatas);
        }

        public string hatasElem(int i)
        {
            return hatasok[i];
        }

        public string mellekhatasElem(int i)
        {
            return mellekhatasok[i];
        }

        public int hatasokMeret()
        {
            return hatasok.Count;
        }

        public int mellekhatasokMeret()
        {
            return mellekhatasok.Count;
        }

        public Gyogyszer()
        {
            nev = "";
            minkor = 0;
        }

    }
}
