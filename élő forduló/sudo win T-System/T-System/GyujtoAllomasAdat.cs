﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T_System
{
    class GyujtoAllomasAdat
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        DateTime letrehozva;

        public DateTime Letrehozva
        {
            get { return letrehozva; }
            set { letrehozva = value; }
        }

        int gyujtoallomas;

        public int Gyujtoallomas
        {
            get { return gyujtoallomas; }
            set { gyujtoallomas = value; }
        }

        int nextbikeGyujtoallomasId;

        public int NextbikeGyujtoallomasId
        {
            get { return nextbikeGyujtoallomasId; }
            set { nextbikeGyujtoallomasId = value; }
        }

        int darab;

        public int Darab
        {
            get { return darab; }
            set { darab = value; }
        }



        public GyujtoAllomasAdat()
        {
        }



        public GyujtoAllomasAdat(int id, DateTime letrehozva, int gyujtoallomas, int nextbikeGyujtoallomasId, int darab)
        {
            this.id = id;
            this.letrehozva = letrehozva;
            this.gyujtoallomas = gyujtoallomas;
            this.nextbikeGyujtoallomasId = nextbikeGyujtoallomasId;
        }



    }
}
