#include <stdio.h>
#include <stdlib.h>

#define DEBUG 0

typedef struct{
 struct VizsgaloParameter* elozo;
 struct VizsgaloParameter* kovetkezo;
  void* adat;
}VizsgaloParameter;

typedef struct{
  void* parameterek; // fontos a sorrend mert az Egyenlo() f�ggv�nyben ezt felt�telezz�k
  unsigned int tomb_meret;

}HivasParameterLista;

typedef struct t_Csomopont{
  unsigned char tipus;
  unsigned int alapertelmezett_eredmeny; // ha nem volt egyez�s a vizsgaland� param�terlist�val
  VizsgaloParameter* vizsgalando_parameterek; // ellen�rz�sre v�r� param�terek
  struct t_Csomopont* elozo; // el�z� csom�pont
  struct t_Csomopont* kovetkezo; // k�vetkez� csom�pont
  struct t_Csomopont** parameter_ugrashelyek; // egyez�s eset�n ugr�si hely/befejez�si, az i. elem az i. param�ter ugr�si helye
}Csomopont;

char Egyenlo(void* param1, void* param2, unsigned char tipus)
{
  switch(tipus){
    case 0: // telefonsz�m [h�v�] hexa
        if(strncmp(param1, param2, sizeof(*param1)) == 0 )
          return 1;
        else return 0;
      break;
    case 1: // telefonsz�m [h�vott] hexa
      if(strncmp(param1, param2, sizeof(*param1)) == 0 )
          return 1;
        else return 0;
      break;
    case 2: // uint32      [cellaID]
      if( ( *(unsigned int*)param1) == ( *(unsigned int*)param2) )
          return 1;
        else return 0;
      break;
    case 3: // bool(char)  [belf�ld/k�lf�ld]
      if( ( *(unsigned int*)param1) == ( *(unsigned int*)param2) )
          return 1;
        else return 0;
      break;
    case 4: // bool(char)  [�tir�ny�t�s]
      if( ( *(unsigned int*)param1) == ( *(unsigned int*)param2) )
          return 1;
        else return 0;
      break;
    case 5: // bool(char)  [internet hozz�f�r�s]
      if( ( *(unsigned int*)param1) == ( *(unsigned int*)param2) )
          return 1;
        else return 0;
      break;
    case 6: // uint16      [IMEI azaonos�t�]
      if( ( *(short int*)param1) == ( *(short int*)param2) )
          return 1;
        else return 0;
      break;
    default:
      return -1; // valamilyen hiba
  }
}

void ujParameter(Csomopont* telefonkozpont, unsigned int hely, unsigned char tipus, void* parameter_mit  )
{
    if(telefonkozpont == NULL) return;
    Csomopont* cs_iter; // el kell menn�nk a 'hely'. csom�pontig
    cs_iter = telefonkozpont;

    unsigned int idx=0;
    while(idx<hely && cs_iter->kovetkezo != NULL)
    {
      cs_iter = cs_iter->kovetkezo;
      idx++;
    }

    if(telefonkozpont->vizsgalando_parameterek == NULL)
    {
      VizsgaloParameter* uj = (VizsgaloParameter*) malloc(sizeof(VizsgaloParameter));
      uj->elozo=NULL;
      uj->kovetkezo=NULL;
      uj->adat=parameter_mit;
      cs_iter->vizsgalando_parameterek = uj;

    }else
     if(telefonkozpont->vizsgalando_parameterek != NULL)
      {
        VizsgaloParameter* p_iter;
        p_iter=telefonkozpont->vizsgalando_parameterek;
        while(p_iter->kovetkezo != NULL) // elmegy�nk a param�ter lista v�g�re, �s hozz�adjuk a param�tert.
        {
          p_iter=p_iter->kovetkezo;
        }
        VizsgaloParameter* uj = (VizsgaloParameter*) malloc(sizeof(VizsgaloParameter));
        uj->elozo=p_iter;
        uj->kovetkezo=NULL;
        uj->adat=parameter_mit;
        p_iter->kovetkezo=uj;

      }
}

void ujCsomopont(Csomopont* telefonkozpont, unsigned char tipus)
{
  if(telefonkozpont == NULL)
  {
    telefonkozpont = (Csomopont*)malloc(sizeof(Csomopont));
    telefonkozpont->alapertelmezett_eredmeny=0;
    telefonkozpont->elozo=NULL;
    telefonkozpont->kovetkezo=NULL;
    telefonkozpont->parameter_ugrashelyek=NULL;
    telefonkozpont->tipus=-1;
    telefonkozpont->vizsgalando_parameterek=NULL;
  }else
  {
    Csomopont* cs_iter;
    cs_iter=telefonkozpont;
    while(cs_iter->kovetkezo!=NULL)
    {
      cs_iter=cs_iter->kovetkezo;
    }
    Csomopont* uj = (Csomopont*) malloc(sizeof(Csomopont));
    uj->alapertelmezett_eredmeny=0;
    uj->kovetkezo=NULL;
    uj->parameter_ugrashelyek=NULL;
    uj->tipus=-1;
    uj->elozo=cs_iter;
    cs_iter->kovetkezo=uj;
    telefonkozpont->vizsgalando_parameterek=NULL;

  }

}

void torlesCsomopont(Csomopont* telefonkozpont, unsigned int hely)
{
  if(telefonkozpont == NULL) return;

  // el jut�s a hely-ik csom�ponthoz
  Csomopont* cs_iter;
  cs_iter = telefonkozpont;
  unsigned int idx=0;
  while(idx<hely &&  cs_iter->kovetkezo!=NULL)
  {
    cs_iter=cs_iter->kovetkezo;
    idx++;
  }

  if(cs_iter->vizsgalando_parameterek != NULL)
  {
    VizsgaloParameter* p_iter;
    VizsgaloParameter* p_kov;
    p_iter=cs_iter->vizsgalando_parameterek;
    if(p_iter->kovetkezo == NULL)
    {
      p_kov=p_iter->kovetkezo;
      while(p_kov!=NULL)
      {
        free(p_kov->elozo);
        p_kov=p_kov->kovetkezo;
      }
      free(p_kov->elozo);
    }

    if(cs_iter->parameter_ugrashelyek != NULL)
    {
      free(cs_iter->parameter_ugrashelyek);
    }
  }
  //csom�pont t�rl�se
    if(cs_iter->kovetkezo == NULL )
    {
      free(cs_iter);
    }
    else
    {
      Csomopont* elotte=cs_iter->elozo;
      Csomopont* utana=cs_iter->kovetkezo;

      elotte->kovetkezo=utana;
      utana->elozo=elotte;

      free(cs_iter);
    }
}

void elModositas(Csomopont* telefonkozpont, unsigned int hanyadik_csomopont, unsigned int hanyadik_el, Csomopont* el_vegpontja)
{
  if(telefonkozpont == NULL) return;

  Csomopont* iter=telefonkozpont;
  unsigned int idx=0;
  while(idx < hanyadik_csomopont &&  iter->kovetkezo != NULL)
  {
    iter=iter->kovetkezo;
    idx++;
  }

  iter->parameter_ugrashelyek[hanyadik_el]=el_vegpontja;
}

int main()
{
    #if DEBUG
      printf("A HivasParameterLista merete: %d\n",sizeof(HivasParameterLista));
      printf("A VizsgaloParameter merete: %d\n",sizeof(VizsgaloParameter));
      printf("A Csomopont merete: %d\n",sizeof(Csomopont));
    #endif



    return 0;
}
