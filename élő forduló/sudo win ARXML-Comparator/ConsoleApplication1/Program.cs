﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Diagnostics;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //XmlTree tree1 = new XmlTree("test.arxml");
            //tree1.Read();
            //XmlTree tree2 = new XmlTree("test0.arxml");
            //tree2.Read();

            XmlTree tree1 = new XmlTree("compare0.arxml");
            tree1.Read();
            XmlTree tree2 = new XmlTree("compare1.arxml");
            tree2.Read();

            Console.WriteLine("Az első szint megegyezik?");
            Comparator.GenerateComparison(tree1, tree2);

            Console.ReadLine();
        }
    }
}
//Becsült átlagértékek mérések alapján:
// ~300 sor (0,3 KB)        = 00:00:00.06
// ~50 000 sor (4,5 MB)     = 00:00:00.08
// ~3 600 000 sor (237 MB)  = 00:00:07.4835736