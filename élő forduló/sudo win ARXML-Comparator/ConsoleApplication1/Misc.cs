﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace ConsoleApplication1
{
    class Misc
    {
        public static void Milyen_XmlNodeType_típusok_vannak_a_fájlban(string fajl)
        {
            Console.WriteLine("Milyen_XmlNodeType_típusok_vannak_a_fájlban? :");
            XmlTextReader reader = new XmlTextReader(fajl);
            List<string> tipusok = new List<string>();

            while (reader.Read())
            {
                if (!tipusok.Contains(reader.NodeType.ToString()))
                    tipusok.Add(reader.NodeType.ToString());
            }

            for (int i = 0; i < tipusok.Count; i++)
                Console.WriteLine(tipusok[i]);

            Console.WriteLine();
        }
        public static void DataSet_fájlba_kiír_és_beolvas()
        {
            DataTable table = new DataTable("Táblanév");

            //Kiírás
            DataSet dataset1 = new DataSet("Datasetnév");
            table.Columns.Add("Életkor", typeof(int));
            table.Columns.Add("Név", typeof(string));
            table.Columns.Add("Éhes", typeof(bool));
            table.Rows.Add(10, "Koko", true);
            table.Rows.Add(20, "Fido", true);
            table.Rows.Add(30, "Alex", false);
            table.Rows.Add(40, "Charles", false);
            table.Rows.Add(50, "Candy", true);
            dataset1.Tables.Add(table);

            string s1 = dataset1.GetXml();
            Console.WriteLine(s1);
            File.WriteAllText("tesztki1.xml", s1);
            
            //Beolvasás
            DataSet dataset2 = new DataSet("tesztbeolvas");
            dataset2.ReadXml("tesztki1.xml", XmlReadMode.Auto);
            string s2 = dataset2.GetXml();
            Console.WriteLine(dataset2.GetXml());

            File.WriteAllText("tesztki2.xml", s2);

            //De a dataset és dataset2 nem azonos objektumok!
        }

        public static void DataSet_DataTable_példa()
        {
            //Minden XmlNodeType.Element egy DataTable???
            DataSet dataset = new DataSet("teszt");
            //dataset.ReadXml

            DataTable table = new DataTable("TáblaNeve");
            table.Columns.Add("Életkor", typeof(int));
            table.Columns.Add("Név", typeof(string));
            table.Columns.Add("Éhes", typeof(bool));
            table.Rows.Add(10, "Koko", true);
            table.Rows.Add(20, "Fido", true);
            table.Rows.Add(30, "Alex", false);
            table.Rows.Add(40, "Charles", false);
            table.Rows.Add(50, "Candy", true);
            dataset.Tables.Add(table);

            // Visualize DataSet
            string s = dataset.GetXml();
            Console.WriteLine(s);
            Console.WriteLine();

            File.WriteAllText("tesztki.xml", s);
        }
        public static void DataView_példa()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("Weight", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Breed", typeof(string));
            table.Columns.Add("Date", typeof(DateTime));

            // Here we add unsorted data to the DataTable and return.
            table.Rows.Add(57, "Koko", "Shar Pei", DateTime.Now);
            table.Rows.Add(130, "Fido", "Bullmastiff", DateTime.Now);
            table.Rows.Add(92, "Alex", "Anatolian Shepherd Dog", DateTime.Now);
            table.Rows.Add(25, "Charles", "Cavalier King Charles Spaniel", DateTime.Now);
            table.Rows.Add(7, "Candy", "Yorkshire Terrier", DateTime.Now);

            // Specify the column to sort on.
            table.DefaultView.Sort = "Weight";

            // Display all records in the view.
            DataView view = table.DefaultView;
            Console.WriteLine("=== Sorted by weight ===");
            for (int i = 0; i < view.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}",
                view[i][0],
                view[i][1],
                view[i][2],
                view[i][3]);
            }

            // Now sort on the Name.
            view.Sort = "Name";

            // Display all records in the view.
            Console.WriteLine("\n=== Sorted by name ===");
            for (int i = 0; i < view.Count; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}",
                view[i][0],
                view[i][1],
                view[i][2],
                view[i][3]);
            }
            Console.WriteLine();
        }
        public static void KonzolraBeolvas(string fajl)
        {
            XmlTextReader reader = new XmlTextReader(fajl);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.XmlDeclaration:
                        Console.WriteLine("<?" + reader.Name + "?>");
                        break;
                    case XmlNodeType.Element:
                        KonzolSzíntÁllít("kék");
                        Console.Write("<" + reader.Name);
                        if (reader.HasAttributes)
                        {
                            KonzolSzíntÁllít("piros");
                            Console.WriteLine();

                            while (reader.MoveToNextAttribute())
                                Console.WriteLine("{0} = \"{1}\"", reader.Name, reader.Value);

                            KonzolSzíntÁllít("kék");
                            Console.WriteLine(">");
                        }
                        else
                            Console.WriteLine(">");
                        break;
                    case XmlNodeType.EndElement:
                        KonzolSzíntÁllít("kék");
                        Console.WriteLine("</" + reader.Name + ">\n\n\n");
                        break;
                    case XmlNodeType.Text:
                        KonzolSzíntÁllít("fekete");
                        Console.WriteLine("{0} ", reader.Value);
                        break;
                    case XmlNodeType.Comment:
                        KonzolSzíntÁllít("zöld");
                        Console.WriteLine(reader.Value);
                        break;

                }
                if (reader.NodeType != XmlNodeType.Whitespace &&
                    reader.NodeType != XmlNodeType.Comment &&
                    reader.NodeType != XmlNodeType.Text)
                    Console.ReadKey();
            }
        }
        public static void KonzolSzíntÁllít(string szín)
        {
            switch (szín)
            {
                case "zöld": Console.ForegroundColor = ConsoleColor.Green; break;
                case "piros": Console.ForegroundColor = ConsoleColor.Red; break;
                case "citromsárga": Console.ForegroundColor = ConsoleColor.Yellow; break;
                case "szürke": Console.ForegroundColor = ConsoleColor.Gray; break;
                case "kék": Console.ForegroundColor = ConsoleColor.Blue; break;
                case "fehér": Console.ForegroundColor = ConsoleColor.White; break;
                case "fekete": Console.ForegroundColor = ConsoleColor.Black; break;
            }
        }
        public static void Load()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
        }
    }
}