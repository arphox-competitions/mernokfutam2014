﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    static class Comparator
    {
        /// <summary>
        /// Összehasonlít két XmlTree objektumot. A két fa sorrendje nem lényeges
        /// </summary>
        public static void GenerateComparison(XmlTree tree1, XmlTree tree2)
        {  
            //első szint vizsgálata:
            Console.WriteLine(SzintVizsgalo(tree1.DataTree, tree2.DataTree));
        }
        private static bool SzintVizsgalo(List<Element> refList, List<Element> newList)
        {
            int i = 0;
            Comparison hasonlitas = new Comparison();

            while (i < refList.Count)
            {
                hasonlitas = Hasonlitanak(refList[i], newList[i]);
                
                if (!hasonlitas.AllTrue)
                    i = refList.Count + 1;
                else
                    i++;
            }
            if (i == refList.Count)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Megvizsgálja a két bemeneti Element-ről, hogy milyen adataik azonosak.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static Comparison Hasonlitanak(Element a, Element b)
        {
            Comparison c = new Comparison();
            if (a.Attributes.Count == b.Attributes.Count)
                c.Attributes_count = true;
            if (a.Children.Count == b.Children.Count)
                c.Children_count = true;
            if (a.ElementName == b.ElementName)
                c.ElementName = true;
            if (a.Text == b.Text)
                c.Text = true;
            return c;
        }
    }
}